#pragma once
#include<deque>

namespace MultplyBigNumber
{
    /// <summary>
    /// ������ �� ������ ������� ����� � ���������� � ������
    /// </summary>
    /// <param name="my_stream">����� �� �����</param>
    /// <param name="mass">������ ��� ������ �� ������</param>
    void read(std::istream& my_stream, std::deque <int>& mass);

    /// <summary>
    /// ��������� ��������� ���� ��������������
    /// </summary>
    void multiply(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);
}
