#pragma once
#include<deque>

namespace last_task_12
{
    /// <summary>
    /// ������ �� ������ ������� ����� � ���������� � ������
    /// </summary>
    /// <param name="my_stream">����� �� �����</param>
    /// <param name="mass">������ ��� ������ �� ������</param>
    void read(std::istream& my_stream, std::deque <long long>& mass);

    /// <summary>
    /// ��������� ��� �������
    /// </summary>
    /// <param name="mass">���������</param>
    /// <param name="mass1">���������</param>
    /// <param name="summa">�����</param>
    void sum(std::deque <long long> mass, std::deque <long long> mass1, std::deque <long long>& summa);
    void _read(std::istream& my_stream, std::deque <long long>& mass);
    void _sum(std::deque <long long> mass, std::deque <long long> mass1, std::deque <long long>& summa);
}
