#pragma once
#include<deque>

namespace Unequil
{
    /// <summary>
    /// ������ �� ������ ������� ����� � ���������� � ������
    /// </summary>
    /// <param name="my_stream">����� �� �����</param>
    /// <param name="mass">������ ��� ������ �� ������</param>
    void read(std::istream& my_stream, std::deque <int>& mass);

    /// <summary>
    /// ��� ��� ��������� ���� ��������
    /// </summary>
    /// <param name="answer">����� �� �������� ������, ��� �����. ���� 0, �� ������ mass, ���� 1, �� ������ mass1, ���� ������, �� mass = mass1</param>
    void com(std::deque <int> mass, std::deque <int> mass1, int& answer);
}
