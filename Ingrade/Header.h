#pragma once
#include <deque>

namespace ingr
{
    /// <summary>
    /// �������� ������ � ����������
    /// </summary>
    /// <param name="mass">���������-������</param>
    /// <param name="index">���������-����������</param>
    /// <param name="summa">������������</param>
    void multiply(std::deque <int> mass, int index, std::deque <int>& summa);
    /// <summary>
    /// ������� 3^index � ���������� � summa
    /// </summary>
    void ingrade(int index, std::deque <int>& summa);
    /// <summary>
    /// ����������� ��� �������
    /// </summary>
    /// <param name="mass">���������</param>
    /// <param name="mass1">���������</param>
    /// <param name="summa">������������</param>
    void multiply_(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);
}
