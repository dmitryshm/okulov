#include <iostream>
#include <deque>
#include "Header.h"

namespace ingr
{
	void ingrade(int index, std::deque <int>& summa)
	{
		summa.clear();
		std::deque <int> mass;
		while (index > 0) { mass.push_front(index % 2); index /= 2; }
		summa.push_front(1);
		for (size_t i = 0; i < mass.size(); i++)
		{
			if (mass[i] == 1) 
			{ 
				multiply_(summa, summa, summa); 
				multiply(summa, 3, summa); 
			}
			else { multiply_(summa, summa, summa); }
		}
	}
	void multiply(std::deque <int> mass, int index, std::deque <int>& summa)
	{
		long long zxc;
		summa.clear();
		int qwer = 0;
		long long zxs;
		long long zzz;
		for (size_t i = 0; i < mass.size(); i++)
		{
			summa.push_back(qwer);
			qwer = 0;
			zxc = static_cast<long long>(mass[i]) * index + summa[summa.size() - 1];
			zxs = static_cast<long long>(zxc);
			if (zxs > 999999999) { zzz = zxc / 1000000000; qwer = static_cast<int>(zzz); zxc %= 1000000000; }
			summa[summa.size() - 1] = static_cast<int>(zxc);
		}
	}
	void multiply_(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa)
	{
		summa.clear();
		long long q = 0;
		long long w = 0;
		long long zxs;
		long long zxx;
		summa.push_back(0);
		long long qwer = 0;
		auto zz = mass1.begin();
		for (size_t i = 0; i < mass1.size(); i++)
		{
			auto zz = mass1.begin() + i;
			for (size_t r = 0; r < mass.size(); r++)
			{
				if (summa.size() <= r + i + 1) { summa.push_back(0); }
				auto ziter = summa.begin() + r + i;
				zxs = static_cast<long long>(*zz) * mass[r] + *(ziter);
				if (zxs > 999999999) { zxx = zxs / 1000000000; *(ziter + 1) += static_cast<int>(zxx); zxs %= 1000000000; }
				*(ziter) = static_cast<int>(zxs);
			}
		}
		for (size_t i = summa.size(); i > 0; --i)
		{
			if (summa[i - 1] == 0 && summa.size() > 1) { summa.pop_back(); }
			else { break; }
		}
	}
}