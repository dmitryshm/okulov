﻿#include <iostream>
#include <cstdlib>
#include<algorithm>
#include "sum.h"
using namespace std;
using namespace Summa;
int main()
{
	deque<int> mass;
	deque <int> mass1;
	deque<int> summa;
	read(cin, mass);
	read(cin, mass1);
	sum(mass, mass1, summa);
	for (size_t i = summa.size(); i > 0; --i)
	{
		if (i != summa.size())
		{
			if (summa[i - 1] > 100) { cout << summa[i - 1]; }
			else if (summa[i - 1] > 10) { cout << "0" << summa[i - 1]; }
			else if (summa[i - 1] > 0) { cout << "00" << summa[i - 1]; }
			else { cout << "000"; }
		}
		else { cout << summa[i - 1]; }
	}
	cout << '\n';
}