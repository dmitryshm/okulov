﻿#include <iostream>
#include <cstdlib>
#include "Solution.h"
#include <deque>

using namespace std;

int main()
{
	deque<int> mass;
	Number_output::read(cin, mass);
	Number_output::output(mass, cout);
	cout << '\n';
}
