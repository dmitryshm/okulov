#pragma once
#include <iostream>
#include <deque>

namespace Number_output
{
    /// <summary>
    /// ������ �� ������ ������� ����� � ���������� � ������
    /// </summary>
    /// <param name="my_stream">����� �� �����</param>
    /// <param name="mass">������ ��� ������ �� ������</param>
    void read(std::istream& my_stream, std::deque <int>& mass);
    /// <summary>
    /// ������ ������ � ���������� ��� � �����
    /// </summary>
    void output(std::deque <int> mass, std::ostream& my_os);
}
