﻿#include <iostream>
#include "../all_cpp/all.h"
using namespace std;

int main()
{
	deque<int> mass;
	int unhappy_y = 0;
	deque<int> summa;
	read_(cin, mass);
	matchroot(mass, summa, unhappy_y);
	for (size_t i = summa.size(); i > 0; --i)
	{
		if (i != summa.size())
		{
			if (summa[i - 1] > 100) { cout << summa[i - 1]; }
			else if (summa[i - 1] > 10) { cout << "0" << summa[i - 1]; }
			else if (summa[i - 1] > 0) { cout << "00" << summa[i - 1]; }
			else { cout << "000"; }
		}
		else { cout << summa[i - 1]; }
	}
	if (unhappy_y != 0) { cout << "," << unhappy_y; }
	cout << '\n';
}