#pragma once
#include<deque>

namespace diff
{
    /// <summary>
    /// ������ �� ������ ������� ����� � ���������� � ������
    /// </summary>
    /// <param name="my_stream">����� �� �����</param>
    /// <param name="mass">������ ��� ������ �� ������</param>
    void read(std::istream& my_stream, std::deque <int>& mass);

    /// <summary>
    /// �������� �� ������ ������� ������ ������ � ������ ������
    /// </summary>
    /// <param name="mass">����������</param>
    /// <param name="mass1">����������</param>
    /// <param name="summa">��������</param>
    /// <param name="dvig">�����</param>
    void difference(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa, int dvig);
}
