﻿#include <iostream>
#include <cstdlib>
#include<algorithm>
#include "difference.h"

using namespace std;
using namespace diff;

int main()
{
	int dvig;
	cin >> dvig;
	cin.ignore();
	deque<int> mass;
	deque <int> mass1;
	deque<int> summa;
	cout << "Input greater number " << '\n';
	read(cin, mass);
	cout << "Input less number " << '\n';
	read(cin, mass1);
	difference(mass, mass1, summa, dvig);
	for (size_t i = summa.size(); i > 0; --i)
	{
		if (i != summa.size())
		{
			if (std::abs(summa[i - 1]) > 100) { cout << std::abs(summa[i - 1]); }
			else if (std::abs(summa[i - 1]) > 10) { cout << "0" << std::abs(summa[i - 1]); }
			else if (std::abs(summa[i - 1]) > 0) { cout << "00" << std::abs(summa[i - 1]); }
			else { cout << "000"; }
		}
		else { cout << summa[i - 1]; }
	}
	cout << '\n';
}