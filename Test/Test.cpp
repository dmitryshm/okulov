#include "CppUnitTest.h"
#include <sstream>
#include <string>
#include "..\Summa\sum.h"
#include "..\Number_output\Solution.h"
#include "..\Unequil\h.h"
#include "..\MultplyBigNumbers\multy.h"
#include "..\difference\difference.h"
#include "..\DivBigNumbers\he.h"
#include "..\factorial\Header.h"
#include "..\Ingrade\Header.h"
#include "..\all_cpp\all.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Test
{
	TEST_CLASS(Test)
	{
	public:
		TEST_METHOD(Number_output)
		{
			std::deque<int> a = {123, 456, 789};
			std::stringstream ss;
			Number_output::output(a, ss);
			std::string s;
			ss >> s;
			Assert::AreEqual("789456123", s.c_str());
			a = {0, 0, 1};
			ss.clear();
			Number_output::output(a, ss);
			ss >> s;
			Assert::AreEqual("1000000", s.c_str());
			a = { 0, 10 };
			ss.clear();
			Number_output::output(a, ss);
			ss >> s;
			Assert::AreEqual("10000", s.c_str());
			a = { 0 };
			ss.clear();
			Number_output::output(a, ss);
			ss >> s;
			Assert::AreEqual("0", s.c_str());
		}
		TEST_METHOD(Summa_1)
		{
			std::deque<int> a, b, c;
			std::stringstream ss;
			ss << "1\n2";
			Summa::read(ss, a);
			Assert::IsTrue(a.size() == 1);
			Assert::IsTrue(a[0] == 1);
			Summa::read(ss, b);
			Assert::IsTrue(b.size() == 1);
			Assert::IsTrue(b[0] == 2);
			Summa::sum(a, b, c);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 3);
		}

		TEST_METHOD(Summa_2)
		{
			std::deque<int> a, b, c;
			std::stringstream ss;
			ss << "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001\n000000000000000000000000000002";
			Summa::read(ss, a);
			Assert::IsTrue(a.size() == 1);
			Assert::IsTrue(a[0] == 1);
			Summa::read(ss, b);
			Assert::IsTrue(b.size() == 1);
			Assert::IsTrue(b[0] == 2);
			Summa::sum(a, b, c);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 3);			
		}

		TEST_METHOD(Summa_3)
		{
			std::deque<int> a, b, c;
			std::stringstream ss;
			a.push_back(1);
			b.push_back(2);
			c.push_back(111111);
			Summa::sum(a, b, c);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 3);
		}

		TEST_METHOD(Summa_4)
		{
			std::deque<int> a, b, c;
			std::stringstream ss;
			ss << "999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999\n1";
			Summa::read(ss, a);
			Assert::IsTrue(a.size() == 51);
			for (auto p : a)
			{
				Assert::IsTrue(p == 999);
			}
			Summa::read(ss, b);
			Assert::IsTrue(b.size() == 1);
			Assert::IsTrue(b[0] == 1);
			Summa::sum(a, b, c);
			Assert::IsTrue(c.size() == 52);
			for (size_t k = 0; k < 51; ++k)
			{
				Assert::IsTrue(c[k] == 0);
			}
			Assert::IsTrue(c[51] == 1);
		}

		TEST_METHOD(Summa_5)
		{
			std::deque<int> a, b, c;
			std::stringstream ss;
			ss << "12345678900000987654321\n98765432100012345";
			Summa::read(ss, a);
			Assert::IsTrue(a.size() == 8);
			Assert::IsTrue(a[0] == 321);
			Assert::IsTrue(a[1] == 654);
			Assert::IsTrue(a[2] == 987);
			Assert::IsTrue(a[3] == 0);
			Assert::IsTrue(a[4] == 900);
			Assert::IsTrue(a[5] == 678);
			Assert::IsTrue(a[6] == 345);
			Assert::IsTrue(a[7] == 12);
			Summa::read(ss, b);
			Assert::IsTrue(b.size() == 6);
			Assert::IsTrue(b[0] == 345);
			Assert::IsTrue(b[1] == 12);
			Assert::IsTrue(b[2] == 100);
			Assert::IsTrue(b[3] == 432);
			Assert::IsTrue(b[4] == 765);
			Assert::IsTrue(b[5] == 98);
			Summa::sum(a, b, c);
			Assert::IsTrue(c.size() == 8);
			Assert::IsTrue(c[0] == 666);
			Assert::IsTrue(c[1] == 666);
			Assert::IsTrue(c[2] == 87);
			Assert::IsTrue(c[3] == 433);
			Assert::IsTrue(c[4] == 665);
			Assert::IsTrue(c[5] == 777);
			Assert::IsTrue(c[6] == 345);
			Assert::IsTrue(c[7] == 12);
		}

		TEST_METHOD(Unequil)
		{
			std::deque<int> a = { 0 };
			std::deque<int> b = { 0 };
			int ans = -1;
			Unequil::com(a, b, ans);
			Assert::IsTrue((ans != 0) && (ans != 1));
			a = { 666, 777, 8 };
			b = { 666, 777, 9 };
			ans = -1;
			Unequil::com(a, b, ans);
			Assert::IsTrue(ans == 1);
			Unequil::com(b, a, ans);
			Assert::IsTrue(ans == 0);
			ans = 1;
			a = b;
			Unequil::com(b, a, ans);
			Assert::IsTrue((ans != 0) && (ans != 1));
		}

		TEST_METHOD(MultplyBigNumbers)
		{
			std::deque<int> a = { 0 };
			std::deque<int> b = { 0 };
			std::deque<int> c = { 1, 2, 3 };
			MultplyBigNumber::multiply(a, b, c);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 0);
			a = { 1 };
			b = { 0 };
			c = { 1, 2, 3 };
			MultplyBigNumber::multiply(a, b, c);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 0);
			a = { 1 };
			b = { 0 };
			c = { 1, 2, 3 };
			MultplyBigNumber::multiply(b, a, c);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 0);
			a = { 1, 123, 543, 234, 56, 65 };
			b = { 0 };
			c = { 1, 2, 3 };
			MultplyBigNumber::multiply(a, b, c);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 0);
			c = { 1, 2, 3 };
			MultplyBigNumber::multiply(b, a, c);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 0);
			a = { 1, 123, 543, 234, 56, 65 };
			b = { 1 };
			c = { 1, 2, 3 };
			MultplyBigNumber::multiply(a, b, c);
			const size_t sza = a.size();
			Assert::IsTrue(c.size() == sza);
			for (size_t k = 0; k < sza; ++k)
			{
				Assert::IsTrue(a[k] == c[k]);
			}
			c = { 1, 2, 3 };
			MultplyBigNumber::multiply(b, a, c);
			Assert::IsTrue(c.size() == sza);
			for (size_t k = 0; k < sza; ++k)
			{
				Assert::IsTrue(a[k] == c[k]);
			}
			a = { 333, 213, 121, 212 };
			b = { 666, 111, 991, 990, 0, 0, 3, 4, 5, 545};
			c = {};
			std::deque<int> d = { 778, 42, 411, 810, 8, 237, 209, 182, 882, 672, 721, 122, 607, 115 };
			MultplyBigNumber::multiply(a, b, c);
			const size_t szd = d.size();
			Assert::IsTrue(c.size() == szd);
			for (size_t k = 0; k < szd; ++k)
			{
				Assert::IsTrue(d[k] == c[k]);
			}
			c = { 1, 2, 3 };
			MultplyBigNumber::multiply(b, a, c);
			Assert::IsTrue(c.size() == szd);
			for (size_t k = 0; k < szd; ++k)
			{
				Assert::IsTrue(d[k] == c[k]);
			}
		}

		TEST_METHOD(difference)
		{
			std::deque<int> a = { 1 };
			std::deque<int> b = { 0 };
			std::deque<int> c = { 12, 34 };
			diff::difference(a, b, c, 0);
			Assert::IsTrue(c.size() == a.size());
			Assert::IsTrue(c[0] == 1);
			a = { 0, 0, 0, 0, 0, 0, 1 };
			c = { 12 };
			diff::difference(a, b, c, 0);
			Assert::IsTrue(c.size() == a.size());
			Assert::IsTrue(c[6] == 1);
			c = { 22 };
			diff::difference(a, b, c, 3);
			Assert::IsTrue(c.size() == a.size());
			Assert::IsTrue(c[6] == 1);
			c = { 33 };
			diff::difference(a, b, c, 6);
			Assert::IsTrue(c.size() == a.size());
			Assert::IsTrue(c[6] == 1);
			c = { 12, 34 };
			a = { 1, 0, 0, 0, 0, 0, 1 };
			b = { 2 };
			std::deque<int> d = { 999, 999, 999, 999, 999, 999 };
			diff::difference(a, b, c, 0);
			size_t szd = d.size();
			Assert::IsTrue(c.size() == szd);
			for (size_t k = 0; k < szd; ++k)
			{
				Assert::IsTrue(c[k] == d[k]);
			}
			c = { 12, 34 };
			diff::difference(a, b, c, 4);
			d = { 1, 0, 0, 0, 998, 999 };
			szd = d.size();
			Assert::IsTrue(c.size() == szd);
			for (size_t k = 0; k < szd; ++k)
			{
				Assert::IsTrue(c[k] == d[k]);
			}
			c = { 12, 34 };
			b = { 1 };
			diff::difference(a, b, c, 6);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 1);
			a = { 333, 444, 555, 666, 777 };
			b = { 222, 333, 444, 555, 666 };
			d = { 111, 111, 111, 111, 111 };
			c = { 12, 34 };
			diff::difference(a, b, c, 0);
			szd = d.size();
			Assert::IsTrue(c.size() == szd);
			for (size_t k = 0; k < szd; ++k)
			{
				Assert::IsTrue(c[k] == d[k]);
			}
		}

		TEST_METHOD(DivBigNumbers)
		{
			std::deque<int> a = { 1 };
			std::deque<int> b = { 1 };
			std::deque<int> c = { 12, 34 };
			std::deque<int> d = { 56, 78, 910 };
			DivBigNumbers::div(a, b, c, d);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 1);
			Assert::IsTrue(d.size() == 1);
			Assert::IsTrue(d[0] == 0);
			a = { 0 };
			c = { 12, 34 };
			d = { 56, 78, 910 };
			DivBigNumbers::div(a, b, c, d);
			Assert::IsTrue(c.size() == 1);
			Assert::IsTrue(c[0] == 0);
			Assert::IsTrue(d.size() == 1);
			Assert::IsTrue(d[0] == 0);
			c = { 12, 34 };
			d = { 56, 78, 910 };
			a = { 232, 323, 434, 222, 1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 0, 0, 122, 22, 443, 76};
			b = { 1 };
			std::deque<int> e = a;
			DivBigNumbers::div(a, b, c, d);
			size_t sze = e.size();
			Assert::IsTrue(c.size() == sze);
			for (size_t k = 0; k < sze; ++k)
			{
				Assert::IsTrue(c[k] == e[k]);
			}
			Assert::IsTrue(d.size() == 1);
			Assert::IsTrue(d[0] == 0);
			a = { 232, 323, 434, 222, 1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 10, 0, 122, 22, 443, 76 };
			b = { 345, 545, 22, 900, 80, 7, 5, 4, 2, 1, 7, 5, 87, 0, 0, 0, 0, 12, 200, 145, 1 };
			c = { 12, 34 };
			d = { 56, 78, 910 };
			DivBigNumbers::div(a, b, c, d);
			MultplyBigNumber::multiply(b, c, e);
			Summa::sum(e, d, e);
			Assert::IsTrue(a.size() == e.size());
			sze = e.size();
			for (size_t k = 0; k < sze; ++k)
			{
				Assert::IsTrue(a[k] == e[k]);
			}
			int ans = -2;
			Unequil::com(b, d, ans);
			Assert::IsTrue(ans == 0);
			a = { 345, 545, 22, 900, 80, 7, 5, 4, 2, 1, 7, 5, 87, 0, 0, 0, 0, 12, 200, 145, 1 }; 
			b = { 232, 323, 434, 222, 1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 10, 0, 122, 22, 443, 76 };
			c = { 12, 34 };
			d = { 56, 78, 910 };
			DivBigNumbers::div(a, b, c, d);
			MultplyBigNumber::multiply(b, c, e);
			Summa::sum(e, d, e);
			Assert::IsTrue(a.size() == e.size());
			sze = e.size();
			for (size_t k = 0; k < sze; ++k)
			{
				Assert::IsTrue(a[k] == e[k]);
			}
			ans = -2;
			Unequil::com(b, d, ans);
			Assert::IsTrue(ans == 0);
		}

		TEST_METHOD(factorial)
		{
			std::deque<int> a = { 2 };
			fact::factorial(100, a);
			Assert::IsTrue(a.size() == 18);
			Assert::IsTrue(a[0] == 0);
			fact::factorial(1000, a);
			Assert::IsTrue(a.size() == 286);
			Assert::IsTrue(a[0] == 0);
			fact::factorial(10000, a);
			Assert::IsTrue(a.size() == 3963);
			Assert::IsTrue(a[0] == 0);
			fact::factorial(100000, a);
			Assert::IsTrue(a.size() == 50731);
			Assert::IsTrue(a[0] == 0);
		}

		TEST_METHOD(ingrade)
		{
			std::deque<int> a = { 2 };
			ingr::ingrade(100, a);
			Assert::IsTrue(a.size() == 6);
			Assert::IsTrue(a[0] == 107522001);
			ingr::ingrade(1000000, a);
			Assert::IsTrue(a.size() == 53014);
			Assert::IsTrue(a[0] == 220000001);
		}

		TEST_METHOD(MatchRoot)
		{
			Assert::IsTrue(false, L"� ������� matchroot ������������ ���������: ���������� ������ �� ��������������� ����� ���� ������ �������� ��������������");
		}

		TEST_METHOD(repeat1)
		{
			std::deque<int> a = { 0 };
			int ans = -1;
			repeat(a, ans);
			Assert::IsTrue(ans == 1);
			a = { 1 };
			ans = -1;
			repeat(a, ans);
			Assert::IsTrue(ans == 1);
			a = { 0, 10 };
			ans = -1;
			repeat(a, ans);
			Assert::IsTrue(ans == 0);
			a = { 95, 95 };
			ans = -1;
			repeat(a, ans);
			Assert::IsTrue(ans == 1);
			a = { 95, 950 };
			ans = -1;
			repeat(a, ans);
			Assert::IsTrue(ans == 0);
			a = { 195, 951 };
			ans = -1;
			repeat(a, ans);
			Assert::IsTrue(ans == 0);
			a = { 854, 221 };
			ans = -1;
			repeat(a, ans);
			Assert::IsTrue(ans == 0);
			a = { 854, 231 };
			ans = -1;
			repeat(a, ans);
			Assert::IsTrue(ans == 1);
		}

		TEST_METHOD(nod1)
		{
			std::deque<int> a = { 0 };
			std::deque<int> b = { 1 };
			std::deque<int> d;
			nod(a, b, d);
			Assert::IsTrue(d.size() == 1);
			Assert::IsTrue(d[0] == 1);
			a = { 15 };
			b = { 695, 2 };
			nod(a, b, d);
			Assert::IsTrue(d.size() == 1);
			Assert::IsTrue(d[0] == 5);
			a = {867, 513, 115, 127, 20};
			b = {571, 395, 292, 10};
			std::deque<int> dd = {709, 299, 1};
			nod(a, b, d);
			size_t szd = d.size();
			Assert::IsTrue(szd == dd.size());
			for (size_t k = 0; k < szd; ++k)
			{
				Assert::IsTrue(d[k] == dd[k]);
			}
			a = { 867, 513, 115, 127, 20 };
			b = { 679, 628, 154, 738, 785, 646, 886, 838 };
			dd = { 709, 299, 1 };
			nod(a, b, d);
			szd = d.size();
			Assert::IsTrue(szd == dd.size());
			for (size_t k = 0; k < szd; ++k)
			{
				Assert::IsTrue(d[k] == dd[k]);
			}
			a = { 104, 247, 939, 705, 319, 766, 79, 146 };
			b = { 559, 932, 604, 496 };
			dd = { 919, 7 };
			nod(a, b, d);
			szd = d.size();
			Assert::IsTrue(szd == dd.size());
			for (size_t k = 0; k < szd; ++k)
			{
				Assert::IsTrue(d[k] == dd[k]);
			}
			dd = a = { 571, 395, 292, 10 };
			b = { 679, 628, 154, 738, 785, 646, 886, 838 };
			nod(a, b, d);
			szd = d.size();
			Assert::IsTrue(szd == dd.size());
			for (size_t k = 0; k < szd; ++k)
			{
				Assert::IsTrue(d[k] == dd[k]);
			}
		}

		TEST_METHOD(nok1)
		{
			std::deque<int> a = { 616, 551, 709, 73, 744, 446, 18 };
			std::deque<int> b = { 919, 7 };
			std::deque<int> n = { 222, 222 };
			std::deque<int> nn = { 104, 247, 939, 705, 319, 766, 79, 146 };
			nok(a, b, n);
			size_t szn = n.size();
			Assert::IsTrue(szn == nn.size());
			for (size_t k = 0; k < szn; ++k)
			{
				Assert::IsTrue(n[k] == nn[k]);
			}
			a = { 559, 932, 604, 496 };
			b = { 681, 484, 243, 689, 1 };
			nn = { 679, 628, 154, 738, 785, 646, 886, 838 };
			nok(a, b, n);
			szn = n.size();
			Assert::IsTrue(szn == nn.size());
			for (size_t k = 0; k < szn; ++k)
			{
				Assert::IsTrue(n[k] == nn[k]);
			}
		}

		TEST_METHOD(amount_divider1)
		{
			std::deque<int> a = { 616, 551, 709, 73, 744, 446, 18 };
			int ans = -1;
			diviver(a, ans);
			Assert::IsTrue(ans == 130);
			a = { 679, 628, 154, 738, 785, 646, 886, 838 };
			diviver(a, ans);
			Assert::IsTrue(ans == 12);
			a = { 264, 395, 64, 204, 439, 572, 183, 694, 180, 893, 108, 280, 727, 474, 15 };
			diviver(a, ans);
			Assert::IsTrue(ans == 780);
		}
	};
}
