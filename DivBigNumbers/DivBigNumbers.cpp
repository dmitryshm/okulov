﻿#include <iostream>
#include <cstdlib>
#include<algorithm>
#include "he.h"
using namespace std;
using namespace DivBigNumbers;
int main()
{
	deque<int> mass;
	deque <int> mass1;
	deque<int> ost;
	deque<int> summa;
	read(cin, mass);
	read(cin, mass1);
	div(mass, mass1, summa, ost);
	cout << "Answer ";
	for (size_t i = summa.size(); i > 0; --i)
	{
		if (i != summa.size())
		{
			if (summa[i - 1] > 100) { cout << summa[i - 1]; }
			else if (summa[i - 1] > 10) { cout << "0" << summa[i - 1]; }
			else if (summa[i - 1] > 0) { cout << "00" << summa[i - 1]; }
			else { cout << "000"; }
		}
		else { cout << summa[i - 1]; }
	}
	cout << '\n';
	cout << "Ostatok ";
	for (size_t i = ost.size(); i > 0; --i)
	{
		if (i != ost.size())
		{
			if (ost[i - 1] > 100) { cout << ost[i - 1]; }
			else if (ost[i - 1] > 10) { cout << "0" << ost[i - 1]; }
			else if (ost[i - 1] > 0) { cout << "00" << ost[i - 1]; }
			else { cout << "000"; }
		}
		else { cout << ost[i - 1]; }
	}
	cout << '\n';
}