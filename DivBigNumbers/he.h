#pragma once
#include<deque>

namespace DivBigNumbers
{
    /// <summary>
    /// ������ �� ������ ������� ����� � ���������� � ������
    /// </summary>
    /// <param name="my_stream">����� �� �����</param>
    /// <param name="mass">������ ��� ������ �� ������</param>
    void read(std::istream& my_stream, std::deque <int>& mass);

    /// <summary>
    /// ��������� ��������� ������� �� ������ � ����� ���������, ������������� ���������, ��� �������������� �����, � ��� ������ � ����� ���������
    /// </summary>
    void multiply(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);

    /// <summary>
    /// ��� ��� ��������� ���� ��������
    /// </summary>
    /// <param name="answer">����� �� �������� ������, ��� �����. ���� 0, �� ������ mass, ���� 1, �� ������ mass1, ���� ������, �� mass = mass1</param>
    void com(std::deque <int> mass, std::deque <int> mass1, int& answer);

    /// <summary>
    /// �������� �� ������ ������� ������ ������
    /// </summary>
    /// <param name="mass">����������</param>
    /// <param name="mass1">����������</param>
    /// <param name="summa">��������</param>
    void difference(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa, int dvig);

    /// <summary>
    /// ����� ��� ������� �� �������� ������� ��������� � ��������
    /// </summary>
    /// <param name="mass">�������</param>
    /// <param name="mass1">��������</param>
    /// <param name="summa">�������</param>
    /// <param name="ost">�������</param>
    void div(std::deque <int> mass, std::deque <int> mass1, std::deque<int>& summa, std::deque <int>& ost);
    /// <summary>
    /// ������� ������� �������������� ���� ��������
    /// </summary>
    void eazy_div(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);
}
