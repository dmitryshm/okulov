﻿#include <iostream>
#include <cstdlib>
#include<algorithm>
#include "Header.h"
using namespace std;
using namespace fact;
int main()
{
	int index;
	deque<int> summa;
	cin >> index;
	factorial(index, summa);
	for (size_t i = summa.size(); i > 0; --i)
	{
		if (i != summa.size())
		{
			if (summa[i - 1] > 100000000) { cout << summa[i - 1]; }
			else if (summa[i - 1] > 10000000) { cout << "0" << summa[i - 1]; }
			else if (summa[i - 1] > 1000000) { cout << "00" << summa[i - 1]; }
			else if (summa[i - 1] > 100000) { cout << "000" << summa[i - 1]; }
			else if (summa[i - 1] > 10000) { cout << "0000" << summa[i - 1]; }
			else if (summa[i - 1] > 1000) { cout << "00000" << summa[i - 1]; }
			else if (summa[i - 1] > 100) { cout << "000000" << summa[i - 1]; }
			else if (summa[i - 1] > 10) { cout << "0000000" << summa[i - 1]; }
			else if (summa[i - 1] > 1) { cout << "00000000" << summa[i - 1]; }
			else { cout << "000000000"; }
		}
		else { cout << summa[i - 1]; }
	}
	cout << '\n';
}