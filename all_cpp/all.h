#pragma once
#include<deque>
/// <summary>
/// ������ �� ������ ������� ����� � ���������� � ������
/// </summary>
/// <param name="my_stream">����� �� �����</param>
/// <param name="mass">������ ��� ������ �� ������</param>
void read(std::istream& my_stream, std::deque <int>& mass);
/// <summary>
/// ������ �� ������ ������� ����� � ���������� � ������ �� 4 �������� � ������ ���������� ��� ��������� ���������� �����
/// </summary>
/// <param name="my_stream">����� �� �����</param>
/// <param name="mass">������ ��� ������ �� ������</param>
void read_(std::istream& my_stream, std::deque <int>& mass);

/// <summary>
/// �������� �� ������ ������� ������ ������ � ������ ������
/// </summary>
/// <param name="mass">����������</param>
/// <param name="mass1">����������</param>
/// <param name="summa">��������</param>
/// <param name="dvig">�����</param>
void difference(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa, int dvig);
/// <summary>
/// ��������� ��������� ������� �� ������ � ����� ���������, ������������� ���������, ��� �������������� �����, � ��� ������ � ����� ���������
/// </summary>
void multiply(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);

/// <summary>
/// ��� ��� ��������� ���� ��������
/// </summary>
/// <param name="answer">����� �� �������� ������, ��� �����. ���� 0, �� ������ mass, ���� 1, �� ������ mass1, ���� ������, �� mass = mass1</param>
void com(std::deque <int> mass, std::deque <int> mass1, int& answer);

/// <summary>
/// ����� ��� ������� �� �������� ������� ��������� � ��������
/// </summary>
/// <param name="mass">�������</param>
/// <param name="mass1">��������</param>
/// <param name="summa">�������</param>
/// <param name="ost">�������</param>
void div(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa, std::deque <int>& ost);
/// <summary>
/// ������� ������� �������������� ���� ��������
/// </summary>
void eazy_div(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);
/// <summary>
/// ��������� ��� �������
/// </summary>
/// <param name="mass">���������</param>
/// <param name="mass1">���������</param>
/// <param name="summa">�����</param>
void sum(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);
/// <summary>
/// ������� ��� �������������� �����
/// </summary>
/// <param name="mass">�������������� �����</param>
/// <param name="mass1">�������������� �����</param>
/// <param name="nod">���</param>
void nod(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& nod);
/// <summary>
/// ������� ��� �������������� �����
/// </summary>
void nok(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& nok);
/// <summary>
/// ��������� ������ � ���������� �����
/// </summary>
/// <param name="mass">������</param>
/// <param name="index">�����, ���� 1, �� ��� ��������, ���� 0, �� ����</param>
void repeat(std::deque <int> mass, int& index);
/// <summary>
/// �������� ��� ������� ����� �����
/// </summary>
void spread_multiply(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);
/// <summary>
/// ��������� ��� ������� ����� �����
/// </summary>
void spread_suma(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);
/// <summary>
/// ��������� ������������ ������ � index-������ �� 16 ������������ � ���������� � �����
/// </summary>
void transfer(std::deque <int> mass, std::deque <int> index, std::ostream& mu);
/// <summary>
/// ������� ���������� ���������
/// </summary>
void diviver(std::deque <int> mass, int& amount);
/// <summary>
/// ������� ������ �� �������
/// </summary>
/// <param name="y">����� ������</param>
/// <param name="unhappy_y">������� ����� ����� ������� �� �������</param>
void matchroot(std::deque <int> mass, std::deque <int>& y, int& unhappy_y);
/// <summary>
/// ����������� ������ c ���������� 10000 � ������������� �����
/// </summary>
void multiply_(std::deque <int> mass, int index, std::deque <int>& summa);
/// <summary>
/// ���������� ������ c ���������� 10000 � ������ 
/// </summary>
void sum_(std::deque <int> mass, int index, std::deque <int>& summa);
/// <summary>
/// ���������� ��� ������� � ����������� 10000
/// </summary>
void sum2(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);
/// <summary>
/// ����������� ��� ������� � ����������� 10000
/// </summary>
void multiply2(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa);
/// <summary>
/// �������� ��� ������� � ����������� 10000 � ������ ������
/// </summary>
void difference2(std::deque <int> mass, std::deque <int> mass1, std::deque <int>& summa, int dvig);